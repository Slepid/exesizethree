import java.util.Arrays;

public class ExeiszeThree {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float arr[] = {4, 7, 2, 9, 0, 1, 9, 12, 56, 78};
		float b = 0.1F;
		
		for (int l = arr.length - 1; l > 0; l--)
		{
			arr[l] = (arr[l] * b)+arr[l];
		}
		
		for(int i = arr.length - 1; i > 0; i--)
		{
			for(int j = 0; j < i; j++)
			{
				if(arr[j] < arr[j+1])
				{
					float tmp = arr[j+1];
					arr[j+1] = arr[j];
					arr[j] = tmp;
				}
			}
		}
		String arrStr = Arrays.toString(arr);
		System.out.println(arrStr);
	}

}